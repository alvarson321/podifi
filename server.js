const express = require('express')
const unirest = require('unirest')

const app = express()
const port = process.env.PORT || 5000

// Get all Episodes from a specific Podcast by its feedurl
app.get('/api/podcast/episodes', (req, res) => {
  const feedurl = req.query.feedurl
  unirest.get(feedurl).end(response => {
    res.status(200).send(response.body)
  })
})

// Get podcast by specific id
app.get('/api/podcast/:id', (req, res) => {
  const podID = req.params.id
  unirest
    .get(`https://itunes.apple.com/lookup?id=${podID}&country=se`)
    .end(response => {
      res.status(200).send(response.body)
    })
})

// Get podcasts from specific category id
app.get('/api/podcast/:category/:amount', (req, res) => {
  const categoryID = req.params.category
  const amount = req.params.amount
  unirest
    .get(
      `https://itunes.apple.com/se/rss/toppodcasts/limit=${amount}/genre=${categoryID}/explicit=true/json`
    )
    .end(response => {
      res.status(200).send(response.body)
    })
})

// Get all podcast categorys
app.get('/api/categorys', (req, res) => {
  unirest
    .get(
      'https://itunes.apple.com/WebObjects/MZStoreServices.woa/ws/genres?id=26&cc=se'
    )
    .end(response => {
      res.status(200).send(response.body)
    })
})

app.listen(port, () => console.log(`Listening on port ${port}`))
