import React, { Component } from 'react'
import '../styles/styles.scss'
import Router from '../Router'

export default class App extends Component {
  render() {
    return (
      <div className="content-wrapper">
        <Router />
      </div>
    )
  }
}
