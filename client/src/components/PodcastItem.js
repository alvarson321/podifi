import React from 'react'
import { Link } from 'react-router-dom'
import slugify from 'slugify'
import LazyLoad from 'react-lazy-load';

const PodcastItem = ({ info, id }) => {
  const podId = info.id.attributes['im:id']
  const podImage300 = info['im:image'][0].label.replace(
    '55x55bb.png',
    '300x300bb-75.jpg'
  )
  const podImage600 = info['im:image'][1].label.replace(
    '60x60bb.png',
    '600x600bb-75.jpg'
  )
  const podImage900 = info['im:image'][2].label.replace(
    '170x170bb.png',
    '900x900bb-75.jpg'
  )
  const podName = info['im:name'].label

  return (
    <Link className="pod-box" to={`/podcast/${podId}/${slugify(podName.toLowerCase())}`}>
      <LazyLoad
        debounce={false}
        offsetVertical={100}
        throttle={500}
      >
        <img
          className="pod-box--image"
          alt={podName}
          data-src={podImage300}
          data-srcset={`${podImage300} 300w,
		        ${podImage600} 600w,
		        ${podImage900} 900w`}
          data-sizes="auto"
          sizes="250px"
          src={podImage300}
          srcSet={`${podImage300} 300w,
		        ${podImage600} 600w,
		        ${podImage900} 900w`}
        />
      </LazyLoad>
    </Link>
  )
}

export default PodcastItem
