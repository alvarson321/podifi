import React, { Component } from 'react'
import PodcastItem from './PodcastItem'
import { Link } from 'react-router-dom'
import slugify from 'slugify'
import { fetchPodcastCategory } from './api'

export default class PopularList extends Component {
  state = {
    podcasts: [],
    loading: true
  }

  async componentDidMount() {
    const { categoryId, amount } = this.props
    const podcasts = await fetchPodcastCategory(categoryId, amount)
    this.setState({
      podcasts,
      loading: false
    })
  }

  render() {
    const { podcasts } = this.state
    const { name } = this.props

    return (
      <div className="row category-list">
        <div className="col-md-12 cateygory-holder">
          <div>
            <h2 className="category__title">{name}</h2>
            <span className="category__subtitle">
              Topp 5 i kategorin {name}
            </span>
          </div>
          <div className="category__show-more">
            <Link to={`/kategori/${slugify(name.toLowerCase())}`}>
              Visa alla
            </Link>
          </div>
        </div>
        <div className="col-md-12 pod-box-content">
          {podcasts.map(podcast => (
            <PodcastItem
              info={podcast}
              key={podcast.id.attributes['im:id']} />
          ))}
        </div>
      </div>
    )
  }
}
