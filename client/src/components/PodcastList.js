import React, { Component } from 'react'
import PodcastItem from './PodcastItem'
import { fetchPodcastCategory } from './api'

export default class PodcastList extends Component {
  state = {
    podcasts: [],
    loading: true
  }

  async componentDidMount() {
    const { categoryId, amount } = this.props
    const podcasts = await fetchPodcastCategory(categoryId, amount)
    this.setState({
      podcasts,
      loading: false
    })
  }

  render() {
    const { podcasts } = this.state
    const { label } = this.props

    return (
      <div className="row category-list">
        <div className="col-md-12">
          {podcasts
            .filter(
              labels =>
                label === labels.category.attributes.label || label === 'All'
            )
            .map(podcast => (
              <PodcastItem
                key={podcast.id.attributes['im:id']}
                info={podcast}
              />
            ))}
        </div>
      </div>
    )
  }
}
