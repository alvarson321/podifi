import React, { Component } from 'react'
import sanitizeHtml from 'sanitize-html'
import moment from 'moment'

class EpisodeItem extends Component {
  state = {
    activeEpisode: false
  }

  render() {
    const { title, description, date, url, handleLoadUrl } = this.props
    const { activeEpisode } = this.state
    const desc = sanitizeHtml(description, {
      allowedTags: [''],
      allowedAttributes: []
    })

    return (
      <div
        className={`col-12 episodes__list__item ${
          activeEpisode ? 'is-open' : ''
          }`}
        onClick={() => this.setState({ activeEpisode: !activeEpisode })}
      >
        <div className="episodes__list__item__top">
          <div className="episodes__list__item__meta">
            {moment(date).format('LL')}
          </div>
          <h3 className="episodes__list__title">{title}</h3>
          <div className="episodes__list__fold">+</div>
          <div className="episodes__list__playbutton">
            <button onClick={() => handleLoadUrl(url, title)}>
              PLAY
            </button>
          </div>
        </div>
        <div
          style={this.state.activeEpisode ? { display: 'block' } : null}
          className="episodes__list__item__desc">
          {desc}
        </div>
      </div>
    )
  }
}

export default EpisodeItem