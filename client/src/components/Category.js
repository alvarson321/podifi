import React, { Component } from 'react'
import PodcastList from './PodcastList'
import { fetchPodcastCategorys } from './api'
import slugify from 'slugify'

export default class Category extends Component {
  state = {
    categorys: [],
    loading: true,
    currentLabel: 'All'
  }

  async componentDidMount() {
    const categorys = await fetchPodcastCategorys()

    this.setState({
      categorys,
      loading: false
    })
  }

  updateLabel(label) {
    this.setState(() => ({
      currentLabel: label
    }))
  }

  render() {
    const { loading, categorys, currentLabel } = this.state
    const categoryNameSlug = this.props.match.params.categoryName
    const currentCategory = categorys.find(
      category => slugify(category.name.toLowerCase()) === categoryNameSlug
    )

    if (loading === true) {
      return <div>Loading...</div>
    } else {
      return (
        <div>
          <div className="container-fluid header-category">
            <div className="row">
              <div className="col-md-12">
                <h1 className="cat-single-title">
                  {currentCategory.name.replace('och', '&')}
                </h1>
                <h2 className="sub-title">
                  Dom 100 populäraste poddarna inom{' '}
                  {currentCategory.name.replace('och', '&')} från iTunes
                </h2>

                <nav className="subgenres">
                  <ul className="filter-button-list">
                    <li className="filter-button">
                      <button
                        style={
                          currentLabel === 'All'
                            ? { backgroundColor: 'rgb(73, 54, 126)', color: '#fff' }
                            : null
                        }
                        onClick={() => this.updateLabel('All')}>
                        Inget filter
                      </button>
                    </li>
                    {currentCategory.genres.map(({ id, name }) => {
                      return (
                        <li className="filter-button" key={id}>
                          <button
                            style={
                              name === currentLabel
                                ? { backgroundColor: 'rgb(73, 54, 126)', color: '#fff' }
                                : null
                            }
                            onClick={() => this.updateLabel(name)}
                          >
                            {name}
                          </button>
                        </li>
                      )
                    })}
                  </ul>
                </nav>
              </div>
            </div>
          </div>

          <div className="container category-collection">
            <PodcastList
              categoryId={currentCategory.id}
              name={currentCategory.name}
              label={currentLabel}
              amount="100"
            />
          </div>
        </div>
      )
    }
  }
}
