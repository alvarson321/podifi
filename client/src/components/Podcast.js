import React, { Component } from 'react'
import { fetchPodcast, fetchPodcastEpisodes } from './api'
import sanitizeHtml from 'sanitize-html'
import Pagination from './common/Pagination'
import Loading from './common/Loading'
import ReactPlayer from 'react-player'
import paginate from '../utils/paginate'
import EpisodeItem from './EpisodeItem'
import Duration from '../utils/Duration'

export default class Podcast extends Component {
  state = {
    podcasts: [],
    episodes: [],
    loading: true,
    pageSize: 8,
    currentPage: 1,
    url: null,
    pip: false,
    playing: false,
    controls: false,
    light: false,
    volume: 0.8,
    muted: false,
    played: 0,
    loaded: 0,
    duration: 0,
    playbackRate: 1.0,
    loop: false,
    episodeTitle: null,
    buffering: false
  }

  load = (url, episodeTitle) => {
    this.setState({
      url,
      episodeTitle,
      played: 0,
      loaded: 0,
      pip: false,
      playing: true
    })
  }

  onDuration = duration => {
    this.setState({ duration })
  }

  onProgress = state => {
    // We only want to update time slider if we are not currently seeking
    if (!this.state.seeking) {
      this.setState(state)
    }
  }

  setVolume = e => {
    this.setState({ volume: parseFloat(e.target.value) })
  }

  toggleMuted = () => {
    this.setState({ muted: !this.state.muted })
  }

  playPause = () => {
    this.setState({ playing: !this.state.playing })
  }

  onSeekMouseDown = e => {
    const x = e.nativeEvent.offsetX
    console.log('Mouse Position:', x)
    console.log('Progressbar width:', this.state.clientWidth)
    const newPlayed = parseFloat(x / this.state.clientWidth)
    this.setState({
      seeking: true,
      newPlayed
    })
  }

  onSeekMouseUp = e => {
    this.setState({
      seeking: false,
      played: this.state.newPlayed
    })
    this.player.seekTo(this.state.newPlayed)
  }

  onBufferStart = () => {
    this.setState({
      buffering: true,
    })
  }

  onBufferEnd = () => {
    this.setState({
      buffering: false,
    })
  }

  ref = player => {
    this.player = player
  }

  handlePageChange = page => {
    this.setState({
      currentPage: page
    })
  }

  async componentDidMount() {
    const podId = this.props.match.params.podId
    const podcasts = await fetchPodcast(podId)

    const feedURL = podcasts[0].feedUrl
    const episodes = await fetchPodcastEpisodes(feedURL)

    this.setState({
      podcasts,
      episodes: episodes.items,
      about: episodes.description,
      loading: false,
      episodeTitle: episodes.items[0].title,
      url: episodes.items[0].media[0].url,
    })

    let { clientWidth } = this.refs.seek_bar;
    this.setState({
      clientWidth
    })
  }

  render() {
    const {
      podcasts,
      episodes,
      about,
      pageSize,
      currentPage,
      loading,
      loaded,
      playing,
      played,
      url,
      controls,
      pip,
      duration,
      volume,
      episodeTitle,
      buffering
    } = this.state

    const { podId } = this.props.match.params

    const podDescription = sanitizeHtml(about, {
      allowedTags: [],
      allowedAttributes: []
    })

    if (loading === true) {
      return <Loading />
    } else {
      const {
        collectionName,
        artworkUrl600,
        primaryGenreName,
        trackCount
      } = podcasts.find(podcast => podcast.collectionId === Number(podId))

      const episodesPag = paginate(episodes, currentPage, pageSize)

      const progress = (played / 1) * 100 + '%'
      const knob = {
        left: `${progress}`
      }
      const progressed = {
        width: `${progress}`
      }

      return (
        <div className="container">

          <div className="player">
            <div className="player-container">

              <div className="innerMedia">
                <div className="meta">
                  <p className="podcast-name">{collectionName}</p>
                  <p className="podcast-title">{episodeTitle}</p>
                </div>
                <div className="volume">
                  <Duration seconds={duration * (1 - played)} />
                  <button onClick={this.playPause}>
                    {playing
                      ? 'Pause'
                      : 'Play'
                    }
                  </button>
                </div>
              </div>

              <div
                className={`seek__holder ${buffering ? 'loading' : ''}`}
                onMouseDown={this.onSeekMouseDown}
                onMouseUp={this.onSeekMouseUp}
                ref="seek_bar"
              >
                <div className="seek__bar__done" style={progressed}></div>
                <div className="seek__knob"
                  style={knob}
                  onDragStart={() => console.log('drag started!')}
                  onDragEnd={() => console.log('drag ended!')}
                >
                </div>
              </div>

            </div>
          </div>

          <ReactPlayer
            ref={this.ref}
            className="pod-player"
            width="100%"
            height="auto"
            url={url}
            playing={playing}
            controls={controls}
            pip={pip}
            onBuffer={this.onBufferStart}
            onBufferEnd={this.onBufferEnd}
            duration={duration}
            volume={volume}
            onProgress={this.onProgress}
            onDuration={this.onDuration}
            loaded={loaded}
          />

          <div className="row">
            <div className="col-md-6">
              <img src={artworkUrl600} alt={collectionName} />
            </div>

            <div className="col-md-6">
              <h1>{collectionName}</h1>
              <p>Antal avsnitt: {trackCount}</p>
              <p>Genre: {primaryGenreName}</p>
              <p>Beskrivning: {podDescription}</p>
            </div>
          </div>

          <div className="row">
            <h3 className="top-title">Avsnitt</h3>
          </div>

          <div className="row">
            {episodesPag.map(item => (
              <EpisodeItem
                key={item.guid}
                title={item.title}
                date={item.date}
                description={item.description}
                url={item.media[0].url}
                handleLoadUrl={this.load}
              />
            ))}
          </div>
          <Pagination
            itemCount={episodes.length}
            currentPage={currentPage}
            pageSize={pageSize}
            onPageChange={this.handlePageChange}
          />
        </div>
      )
    }
  }
}
