import React from 'react'
import _ from 'lodash'

const Pagination = props => {
  const { itemCount, pageSize, currentPage, onPageChange } = props;

  const pagesCount = Math.ceil(itemCount / pageSize);
  if (pagesCount === 1) return null;
  const pages = _.range(1, pagesCount + 1)

  return (
    <nav>
      <ul>
        {pages.map(page => (
          <li key={page} className={page === currentPage ? 'page-item active' : 'page-item'}>
            <a href="/" className="link" onClick={() => onPageChange(page)}>
              {page}
            </a>
          </li>
        ))}
      </ul>
    </nav>
  )
}

export default Pagination
