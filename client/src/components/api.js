import Feed from '@economia/feed-to-json-promise'

// Get podcasts from specific category id
export async function fetchPodcastCategory(categoryId, amount) {
  const response = await fetch(`/api/podcast/${categoryId}/${amount}`)
  const podcasts = await response.json()
  console.log('Request is made.')
  return podcasts.feed.entry
}

// Get all podcast categorys
export async function fetchPodcastCategorys() {
  const response = await fetch('/api/categorys')
  const result = await response.json()

  const category = result['26'].subgenres

  const categorys = Object.keys(category).map(genre => ({
    id: category[genre].id,
    name: category[genre].name,
    genres: Object.keys(category[genre].subgenres || []).map(subgenre => ({
      name: category[genre].subgenres[subgenre].name,
      id: category[genre].subgenres[subgenre].id
    }))
  }))

  return categorys
}

// Get podcast by specific id
export async function fetchPodcast(podId) {
  const response = await fetch(`/api/podcast/${podId}`)
  const podcasts = await response.json()

  return podcasts.results
}

// Get all Episodes from a specific Podcast by its feedurl
export async function fetchPodcastEpisodes(feedUrl) {
  const feed = new Feed()
  const episodes = await feed.load(`/api/podcast/episodes?feedurl=${feedUrl}`)
  return episodes
}
