import React, { Component } from 'react'
import PopularList from './PopularList'
import Button from './common/Button'
import { fetchPodcastCategorys } from './api'

export default class Home extends Component {
  state = {
    categorys: [],
    loading: true
  }

  async componentDidMount() {
    const categorys = await fetchPodcastCategorys()

    this.setState({
      categorys,
      loading: false
    })
  }

  render() {
    const podcastCategories = this.state.categorys

    return (
      <div>
        <div className="info-block info-wrapper">
          <div className="container">
            <div className="row">
              <div className="col-md-5 offset-md-1">
                <div className="info__panel">
                  <span className="title__ingress text-color-white">Hej, välkommen till Podifi,</span>
                  <h1 className="text-color-white service__description">Lyssna på dina favorit podcasts.</h1>
                  <Button type="filled">Utforska podcasts</Button>
                </div>
              </div>

              <div className="col-md-5 offset-md-1">
                <div className="info__panel">
                  <span className="title__ingress text-color-bruno">Logga in eller skapa ett konto för att</span>
                  <h1 className="text-color-bruno service__description">Smidigt att samla dina podcasts.</h1>
                  <Button type="filled">Skapa ett konto</Button>
                  <Button type="outlined">Logga in</Button>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div className="container">
          <div className="row get-started d-flex">
            <div className="col kuk">
              <h2>Osäker på du vill lyssna på?</h2>
              <p>Inga problem, nedan kan du ta del av mängder av podcasts. Vet du ungefär vad du vill lyssna på? Välj någon kategori nedan.</p>
            </div>
          </div>
        </div>


        <div className="container">
          {podcastCategories.map(category => (
            <PopularList
              key={category.id}
              categoryId={category.id}
              name={category.name}
              amount="5"
            />
          ))}
        </div>
      </div>
    )
  }
}
