import React from 'react'
import ReactDOM from 'react-dom'
import App from './components/App'
import Nav from './components/Nav'
import Footer from './components/Footer'
import { BrowserRouter as Router } from 'react-router-dom'

const app = (
  <Router>
    <Nav />
    <App />
    <Footer />
  </Router>
)

ReactDOM.render(app, document.getElementById('root'))
