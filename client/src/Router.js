import React from 'react'
import {Route, Switch} from 'react-router-dom'
import Home from './components/Home'
import Podcast from './components/Podcast'
import Category from './components/Category'

const Router = () => (
  <Switch>
    <Route path="/" exact component={Home} />
    <Route path="/podcast/:podId/:podName" component={Podcast} />
    <Route exact path="/kategori/:categoryName" component={Category} />
  </Switch>
)

export default Router
